#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <vector>
#include <string>
#include <QtWidgets>
#include "sturing.h"
#include "ui.h"

using namespace std;

class Controller : public QObject
{
    Q_OBJECT
private:
    STuring* turing;
    UI* ui;
public:
    explicit Controller(QObject *parent = 0);

signals:

public slots:
    void tmRun();
    void setRunable(bool);
    void changeSpeed(int n);
    void tmStop();
};

#endif // CONTROLLER_H
